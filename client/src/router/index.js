import { createRouter, createWebHistory } from 'vue-router'
import Products from '../views/Products.vue'
import AddProduct from "../views/AddProduct.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Products
    },
    {
        path: '/addproduct',
        name: 'AddProduct',
        component: AddProduct
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router