<?php
declare(strict_types=1);

namespace ScandiwebShop\models;

use ScandiwebShop\src\Application\DbModel;

class Product extends DbModel
{
    public string $sku = '';
    public string $name = '';
    public float $price = 0;
    public mixed $dimensions = '';

    public function tableName(): string
    {
        return "products";
    }

    public function save(): bool
    {
        $this->dimensions = json_encode($this->dimensions);
        return parent::saveToDB();
    }

    public function attributes(): array
    {
        return ['sku','name','price','dimensions'];
    }
}