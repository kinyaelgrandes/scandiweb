<?php
declare(strict_types=1);

namespace ScandiwebShop\controllers;

use ScandiwebShop\models\Product;
use ScandiwebShop\src\Application\Application;
use ScandiwebShop\src\Application\Request;

class ProductsController
{
    /**
     * return all products as json
     */
    public function getProducts(Request $request)
    {
        $productModel = new Product();
        if ($request->isGet()) {
            $products = $productModel->findAll();
            Application::$app->response->json($products, 200);
        }
    }

    /**
     * handle products save functionality
     */
    public function handleProduct(Request $request)
    {
        $productModel = new Product();
        if ($request->isPost()) {
            $productModel->loadData($request->getBody());
            $productModel->save();
            Application::$app->response->json(['success' => 'Product saved successfully'], 200);
        }
    }

    /**
     * Deleting products
     */
    public function destroyProducts(Request $request)
    {
        $productModel = new Product();
        if ($request->isDelete()) {
            $productModel->deleteChecked($request->getBody());
            Application::$app->response->json(['success' => 'Product(s) has been deleted successfully'], 200);
        }
    }


}