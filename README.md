# Scandiweb Junior Developer Test

This Application is a test for the [Junior Developer](https://www.notion.so/Junior-Developer-Test-Task-1b2184e40dea47df840b7c0cc638e61e)
role at [Scandiweb](https://scandiweb.com/).

### : application: Environment Dependencies

- [`nvm`](https://github.com/nvm-sh/nvm) - For managing the version of node.
    - It is recommend to setup [`nvm`'s deeper shell integration](https://github.com/nvm-sh/nvm#deeper-shell-integration),
      so that the node version is automatically set based on the `.nvmrc` config under `client` directory.
- [`composer`](https://getcomposer.org/) - For managing composer dependencies
- #### `PHP-8` - For running the server

<br/>

### How To Install the Backend

Install composer dependencies using the composer install command from the root of the project.
This will install all composer dependencies listed in the root `composer.json`.

```bash
composer install
```

### :Database setup

Copy the `.env.example `file and make the required database configuration changes in the `.env` file

```bash
cp .env.example .env
```

After setting up your database credentials run the following script on the root of the project to create `products` table in your database.

```bash
php dbSetup.php
```
### How To Run Backend API Locally

Navigate to `public` directory to run the following script to fire-up  the backend Api on port :`8000`

```bash
php -S 127.0.0.1:8000 
```

After running the above command visit the following url http://127.0.0.1:8000 to see the following text 

> ### Yeiii 🎉 , backend setup done : )

<br/>

### How To Install the Frontend

Navigate to the client directory while on the root directory and install node dependencies using the npm install command.
This will install all node dependencies listed in the client directory `package.json`.

```bash
cd client && npm install
```
### How To Run the Frontend Locally

To run the codebase locally use one of the following script, from the `client` directory:

```bash
npm run dev
```
The script will run the frontend codebase locally at [localhost:3000](http://localhost:3000).
