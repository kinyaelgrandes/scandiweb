<?php
declare(strict_types=1);

namespace ScandiwebShop\src\Application;

class Database
{
    public \PDO $pdo;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';
        $this->pdo = new \PDO($dsn, $user, $password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Creates Product's table
     * @return bool
     */
    public function createTables(): bool
    {
        $statement = $this->pdo->prepare("CREATE TABLE IF NOT EXISTS products(
            id INT AUTO_INCREMENT PRIMARY KEY ,
            name VARCHAR(255) NOT NULL,
            sku VARCHAR(255) NOT NULL,
            price DECIMAL(6,2) NOT NULL,
            dimensions JSON NOT NULL, 
            created_at TIMESTAMP DEFAULT  CURRENT_TIMESTAMP
        ) ENGINE=INNODB;");

        $statement->execute();
        return true;
    }

}