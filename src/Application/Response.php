<?php
declare(strict_types=1);

namespace ScandiwebShop\src\Application;

class Response
{
   public function json(mixed $data,int $code)
   {
       header('Content-Type: application/json; charset=utf-8');
       http_response_code($code);
       echo json_encode($data);
   }
}