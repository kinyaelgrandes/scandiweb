<?php
declare(strict_types=1);

namespace ScandiwebShop\src\Application;

class Request
{
    public function getPath(): string
    {
        $path = $_SERVER['REQUEST_URI'] ?? false;
        $position = strpos($path, '?');

        if ($position === false) {
            return $path;
        }
        return substr($path, 0, $position);
    }

    public function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function isGet(): bool
    {
        return $this->getMethod() === 'get';
    }

    public function isPost(): bool
    {
        return $this->getMethod() === 'post';
    }

    public function isDelete(): bool
    {
        return $this->getMethod() === 'delete';
    }

    /**
     * @return mixed
     */
    public function getBody(): mixed
    {
        return json_decode(file_get_contents('php://input'), true);
    }
}