<?php
declare(strict_types=1);

namespace ScandiwebShop\src\Application;

abstract class DbModel extends Model
{
    /**
     * Inserts into values into the database
     * @return bool
     */
    public function saveToDB(): bool
    {
        $tableName = $this->tableName();
        $attributes = $this->attributes();
        $values = array_map(fn($attr) => ":$attr", $attributes);

        $statement = self::prepare("INSERT INTO $tableName(" . implode(',', $attributes) . ") 
            VALUES(" . implode(',', $values) . ")");

        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }

        $statement->execute();
        return true;
    }

    abstract public function tableName(): string;

    abstract public function attributes(): array;

    public static function prepare($sql)
    {
        return Application::$app->database->pdo->prepare($sql);
    }

    /**
     * Fetches all the products from the database
     * @return array
     */
    public function findAll(): array
    {
        $tableName = $this->tableName();
        $pdo = Application::$app->database->pdo;

        $statement = $pdo->query("SELECT * FROM $tableName");
        $results = $statement->fetchAll($pdo::FETCH_ASSOC);

        $sanitizedResults = [];
        foreach ($results as $result) {
            $firstValues = array_slice($result, 0, 4);
            $assocDimensions = json_decode($result["dimensions"], true);

            $newResult = array_merge($firstValues, ["dimensions" => $assocDimensions]);
            array_push($sanitizedResults, $newResult);
        }

        return $sanitizedResults;
    }

    /**
     * Mass deletes checked products from the database
     * @param $ids
     */
    public function deleteChecked($ids)
    {
        $tableName = $this->tableName();

        $idsValue = implode(',',$ids);

        $statement = self::prepare("DELETE FROM $tableName WHERE id IN ($idsValue)");
        $statement->execute();
    }
}