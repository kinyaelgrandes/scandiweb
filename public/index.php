<?php

declare(strict_types=1);

/**
 * @author Mumbi Francis Kinyanjui <mumbifranciskinyanjui@gmail.com>
 */

use ScandiwebShop\controllers\ProductsController;
use ScandiwebShop\src\Application\Application;

//Application's config
require_once __DIR__ . "/../vendor/autoload.php";
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
$config = [
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'user' => $_ENV['DB_USERNAME'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];
$app = new Application($config);

//routes
$app->router->get("/", function () {
    return "Yeiii 🎉 , backend setup done : )";
});
$app->router->get("/api/v1/products", [ProductsController::class, 'getProducts']);
$app->router->post("/api/v1/addproduct", [ProductsController::class, 'handleProduct']);
$app->router->delete("/api/v1/deleteproducts", [ProductsController::class, 'destroyProducts']);

$app->cors();
$app->fireUp();